from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
import math
import time
import numpy as np
import tensorflow as tf
import os
import json
import csv
from model import select_model, get_checkpoint
from utils import *
import pandas as pd
import cv2
import dlib
import logging
import numpy as np
import time
# initialise variables
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from werkzeug import secure_filename
import boto3 # AWS Packages
from boto3.s3.transfer import S3Transfer # AWS Packages
from config import *
import base64
from io import StringIO, BytesIO
from PIL import Image, ImageFile

#Flask api initialise
app = Flask(__name__)

s3_transfer = S3Transfer(boto3.client('s3', region_name='us-east-1',aws_access_key_id = AWS_prajwal_credetials['aws_access_key_id'], aws_secret_access_key = AWS_prajwal_credetials['aws_secret_access_key']))

# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['txt'])#, 'jpg', 'jpeg'])

RESIZE_FINAL = 227
GENDER_LIST =['M','F']
AGE_LIST = ['(0, 2)','(4, 6)','(8, 12)','(15, 20)','(25, 32)','(38, 43)','(48, 53)','(60, 100)']
MAX_BATCH_SZ = 128

tf.app.flags.DEFINE_string('f', '', 'kernel')
tf.app.flags.DEFINE_string('device_id', '/cpu:0', 'What processing unit to execute inference on')

tf.app.flags.DEFINE_string('target', '/app_age_gender_detection/result_gender.csv', 'CSV file containing the filename processed along with best guess and score')

tf.app.flags.DEFINE_string('checkpoint', 'checkpoint','Checkpoint basename')

tf.app.flags.DEFINE_string('model_type', 'inception', 'Type of convnet')

tf.app.flags.DEFINE_string('requested_step', '', 'Within the model directory, a requested step to restore e.g., 9000')

tf.app.flags.DEFINE_boolean('single_look', False, 'single look at the image or multiple crops')
#
# tf.app.flags.DEFINE_string('face_detection_model', '/app_age_gender_detection/face_landmarks/models/haarcascade_frontalface_default.xml', 'Do frontal face detection with model specified')
#
# tf.app.flags.DEFINE_string('face_detection_type', 'cascade', 'Face detection model type (yolo_tiny|cascade)')

FLAGS = tf.app.flags.FLAGS

def load_graph(frozen_graph_filename):
    # We load the protobuf file from the disk and parse it to retrieve the
    # unserialized graph_def
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    # Then, we import the graph_def into a new Graph and returns it
    with tf.Graph().as_default() as graph:
        # The name var will prefix every op/nodes in your graph
        # Since we load everything in a new graph, this is not needed
        tf.import_graph_def(graph_def)

    # for op in graph.get_operations():
    #     #if 'softmax' in op.name:
    #    print(op.name)
    return graph


# load graph for age
frozen_graph_age = '/app_age_gender_detection/models/22801_age/frozen_model.pb'
graph_age = load_graph(frozen_graph_age)
#result_age = get_age(image_frame)

# load graph for gender
frozen_graph_gender = '/app_age_gender_detection/models/21936_gender/frozen_model.pb'
graph_gender = load_graph(frozen_graph_gender)

# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

def compress_img(size, filepath, size1, size2, size3, im):

        # conditions to compress image
        if size > size1 and size < size2:
            im.save(filepath, "JPEG", quality=30, optimize=True, progressive=True)

        elif size > size2 and size < size3:
            im.save(filepath, "JPEG", quality=50, optimize=True, progressive=True)

        else:
            im.save(filepath, "JPEG", quality=30, optimize=True, progressive=True)

        return 0


# API set up  ---------------------------------------------------------------------------------------------
@app.route('/age_gender_Test')
def age_gender_Test():
    return 'Age Gender API working'


@app.route('/age_gender', methods=['POST'])
def age_gender():

    if request.method == 'POST':
        logging.warning('Start of Algo')
        result = 'Error in processing, please ensure the format of the file is as per the documentation.'
        # Get the name of the uploaded file
        logging.warning(request)

        size1 = 1000000
        size2 = 2000000
        size3 = 3000000

        try:

            if request.is_json == False: # this section is supporting image file processing

                file = request.files['file']

                if allowed_file(file.filename):
                    logging.warning(file.filename)

                    # Make the filename safe, remove unsupported chars
                    filename = secure_filename(file.filename)
                    filename = file.filename
                    filename_img = filename.rsplit('.', 1)[0] + '.jpeg'
                    filepath = 'uploads/' + filename_img
                    logging.warning(filename_img)
                    img_data_base64 = file.stream.read()

                    #write image with reduced quality
                    im = Image.open(BytesIO(base64.b64decode(img_data_base64)))
                    im.save(filepath, "JPEG", quality=100, optimize=True, progressive=True)
                    logging.warning('img_data write complete')

                else:
                    result = "Not one of the allowed file extensions. Please try another file."

            elif request.is_json:  # this section is image file in json format processing

                   # read image data or URL
                    img_json = pd.DataFrame(request.json)
                    logging.warning('dataframe read')
                    img_file_name= img_json['image_file_name'][0] #image file name
                    img_url_data = img_json['image_data'][0] #image data

                    filename_img = img_file_name + '.jpeg'
                    filepath = 'uploads/' + filename_img

                    im = Image.open(BytesIO(base64.b64decode(img_url_data)))
                    im.save(filepath, "JPEG", quality=100, optimize=True, progressive=True)
                    logging.warning('img_data write complete')


            # Upload a image file to S3
            #s3_transfer.upload_file(filepath, 'api.data', str(filename_img))
            #logging.warning('image uploaded to S3')

            # read image from drive
            logging.warning(filepath)
            im = Image.open(filepath)
            size = os.stat(filepath).st_size

            #filepath = "/app_age_gender_detection/pics/anusha_1.jpg"
            #compress_img(size, filepath, size1, size2, size3, im)
            result_age = get_age(filepath)
            result_gender = get_gender(filepath)
            #result = "("+str(result_age[0])+"," + str(result_gender[0])+")"
            result = str({"Age":str(result_age[0][0]) ,"Age Confidence":str(result_age[0][1]),"Gender":str(result_gender[0][0]),"Gender Confidence":str(result_gender[0][1])})

            os.remove(filepath)#remove the file
            logging.warning('File delete')
            logging.warning(result)


        except Exception as e:
               logging.warning(e)
               result = str(e) #'Error while processing the image.'

        return result


def one_of(fname, types):
    return any([fname.endswith('.' + ty) for ty in types])

def resolve_file(fname):
    if os.path.exists(fname): return fname
    for suffix in ('.jpg', '.png', '.JPG', '.PNG', '.jpeg'):
        cand = fname + suffix
        if os.path.exists(cand):
            return cand
    return None


def classify_many_single_crop(sess, label_list, softmax_output, coder, images, image_files, writer):
    try:
        num_batches = math.ceil(len(image_files) / MAX_BATCH_SZ)
        pg = ProgressBar(num_batches)
        for j in range(num_batches):
            start_offset = j * MAX_BATCH_SZ
            end_offset = min((j + 1) * MAX_BATCH_SZ, len(image_files))

            batch_image_files = image_files[start_offset:end_offset]
            print(start_offset, end_offset, len(batch_image_files))
            image_batch = make_multi_image_batch(batch_image_files, coder)
            batch_results = sess.run(softmax_output, feed_dict={images:image_batch.eval()})
            batch_sz = batch_results.shape[0]
            for i in range(batch_sz):
                output_i = batch_results[i]
                best_i = np.argmax(output_i)
                best_choice = (label_list[best_i], output_i[best_i])
                print('Guess @ 1 %s, prob = %.2f' % best_choice)
                if writer is not None:
                    f = batch_image_files[i]
                    writer.writerow((f, best_choice[0], '%.2f' % best_choice[1]))
            pg.update()
        pg.done()

    except Exception as e:
        print(e)
        print('Failed to run all images')

def classify_one_multi_crop(sess, label_list, softmax_output, coder, images, image_file, writer):
    try:

        print('Running file %s' % image_file)
        #print('Coder:{}'.format(coder))
        image_batch = make_multi_crop_batch(image_file, coder)
        #print('image_batch:{}'.format(image_batch))
        batch_results = sess.run(softmax_output, feed_dict={images:image_batch.eval()})
        #print('batch_results:{}'.format(batch_results))
        output = batch_results[0]
        batch_sz = batch_results.shape[0]

        for i in range(1, batch_sz):
            output = output + batch_results[i]

        output /= batch_sz
        best = np.argmax(output)
        best_choice = (label_list[best], output[best])
        print('Guess @ 1 %s, prob = %.2f' % best_choice)

        nlabels = len(label_list)
        if nlabels > 2:
            output[best] = 0
            second_best = np.argmax(output)
            print('Guess @ 2 %s, prob = %.2f' % (label_list[second_best], output[second_best]))

        if writer is not None:
            writer.writerow((image_file, best_choice[0], '%.2f' % best_choice[1]))

        return best_choice
    except Exception as e:
        print(e)
        print('Failed to run image %s ' % image_file)


def list_images(srcfile):
    with open(srcfile, 'r') as csvfile:
        delim = ',' if srcfile.endswith('.csv') else '\t'
        reader = csv.reader(csvfile, delimiter=delim)
        if srcfile.endswith('.csv') or srcfile.endswith('.tsv'):
            print('skipping header')
            _ = next(reader)

        return [row[0] for row in reader]



def main_pb(img_name, graph, class_type):  # pylint: disable=unused-argument
    result_all = []
    path = img_name
    print(path)
    files = [img_name]


    # if FLAGS.face_detection_model:
    #     print('Using face detector (%s) %s' % (FLAGS.face_detection_type, FLAGS.face_detection_model))
    #     face_detect = face_detection_model(FLAGS.face_detection_type, FLAGS.face_detection_model)
    #     face_files, rectangles = face_detect.run(path)
    #     files += face_files
    #     print("Files {}".format(files))
    #     print(type(files))

    if len(files) > 0:

        with tf.Session(graph=graph) as sess:
            label_list = AGE_LIST if class_type == 'age' else GENDER_LIST
            nlabels = len(label_list)
            images = graph.get_tensor_by_name('import/Placeholder:0')
            #images = tf.placeholder(tf.float32, [None, RESIZE_FINAL, RESIZE_FINAL, 3])
            image_files = list(filter(lambda x: x is not None, [resolve_file(f) for f in files]))
            coder = ImageCoder()
            # print("Nodes:")
            # for n in tf.get_default_graph().as_graph_def().node:
            #     print(n.name)
            softmax_output = graph.get_tensor_by_name('import/Softmax:0')#tf.nn.softmax(logits)
            print(softmax_output)

            #<<<<<<<<<<<<<<<<< Support a batch mode if no face detection model>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if len(files) == 0:

                files.append(img_name)

            # if len(files) == 0:
            #     if (os.path.isdir(FLAGS.filename)):
            #         for relpath in os.listdir(FLAGS.filename):
            #             abspath = os.path.join(FLAGS.filename, relpath)
            #
            #             if os.path.isfile(abspath) and any([abspath.endswith('.' + ty) for ty in ('jpg', 'png', 'JPG', 'PNG', 'jpeg')]):
            #                 print(abspath)
            #                 files.append(abspath)
            #     else:
            #         files.append(FLAGS.filename)
            #         # If it happens to be a list file, read the list and clobber the files
            #         if any([FLAGS.filename.endswith('.' + ty) for ty in ('csv', 'tsv', 'txt')]):
            #             files = list_images(FLAGS.filename)

            writer = None
            output = None
            if FLAGS.target:
                print('Creating output file %s' % FLAGS.target)
                output = open(FLAGS.target, 'w')
                writer = csv.writer(output)
                writer.writerow(('file', 'label', 'score'))
            image_files = list(filter(lambda x: x is not None, [resolve_file(f) for f in files]))
            print("image_files: {}".format(image_files))
            if FLAGS.single_look:
                classify_many_single_crop(sess, label_list, softmax_output, coder, images, image_files, writer)

            else:
                for image_file in image_files:
                    #image_batch = make_multi_crop_batch(image_file, coder)
                    result = classify_one_multi_crop(sess, label_list, softmax_output, coder, images, image_file, writer)
                    result_all.append(result)
                return result_all
            if output is not None:
                output.close()
    else:
        result_all = 'Un Indentified'
        return result_all





def get_age(file_name):
    class_type = 'age'
    result_age = main_pb(file_name, graph_age, class_type)
    return result_age

def get_gender(file_name):
    class_type = 'gender'
    result_age = main_pb(file_name, graph_gender, class_type)
    return result_age

#print(type(get_age('pics/prajwal_1.jpg')))

if __name__ == '__main__':
   app.run(host="0.0.0.0", debug=True, port=5017)
